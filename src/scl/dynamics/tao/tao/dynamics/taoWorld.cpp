/* Copyright (c) 2005 Arachi, Inc. and Stanford University. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/** Edited 2013-08-21 : Samir Menon <smenon@stanford.edu>
 */

#include "taoWorld.h"
#include <tao/dynamics/taoJoint.h>
#include <tao/dynamics/taoNode.h>
#include <tao/dynamics/taoDynamics.h>

taoWorld::~taoWorld()
{
	taoNodeRoot* r;
	while (root_node_)
	{	
		r = root_node_;
		root_node_ = root_node_->getNext();
		delete r;
	}
}

void taoWorld::update(const deFloat time, const deFloat dt, const deInt n)
{
	deInt i = 0;
	while (i < n)
	{
		taoNodeRoot* r = root_node_;
		while (r)
		{
			if (!r->getIsFixed())
			{
				taoDynamics::fwdDynamics(r, &_gravity);
				taoDynamics::integrate(r, dt);
				taoDynamics::updateTransformation(r);
			}
			r = r->getNext();
		}
		i++;
	}
}

void taoWorld::simulate(const deFloat dt)
{
	taoNodeRoot* r = root_node_;
	while (r)
	{
		if (!r->getIsFixed())
		{
			taoDynamics::fwdDynamics(r, &_gravity);
			taoDynamics::integrate(r, dt);
		}
		r = r->getNext();
	}
}

void taoWorld::updateTransformation()
{
	taoNodeRoot* r = root_node_;
	while (r)
	{
		if (!r->getIsFixed())
		{
			taoDynamics::updateTransformation(r);
		}
		r = r->getNext();
	}
}

void taoWorld::addRoot(taoNodeRoot* r, const deInt id)
{
	r->setNext(root_node_);
	root_node_ = r;

	r->setID(id);
	r->setGroup(this);
}

taoNodeRoot* taoWorld::removeRoot(const deInt id)
{
	taoNodeRoot* n, *g = root_node_;
	if (g && g->getID() == id)
	{
		root_node_ = g->getNext();
		g->setNext(NULL);
		return g;
	}
	else
	{
		while (g->getNext())
		{
			if (g->getNext()->getID() == id)
			{
				n = g->getNext();
				g->setNext(n->getNext());
				n->setNext(NULL);
				return n;
			}
			g = g->getNext();
		}
	}
	return NULL;
}

taoNodeRoot* taoWorld::findRoot(const deInt id)
{
	taoNodeRoot* r = root_node_;
	while (r)
	{
		if (r->getID() == id)
			return r;
		r = r->getNext();
	}
	return NULL;
}

taoNodeRoot* taoWorld::unlinkFixed(taoNodeRoot* root, taoNode* node)
{
	taoNodeRoot* r = new taoNodeRoot(*node->frameGlobal());

	node->unlink();

	if (!root->getDChild())
		delete removeRoot(root->getID());

	deFrame home;
	home.identity();

	node->link(r, &home);

	node->addABNode();

	taoDynamics::initialize(r);

	addRoot(r, -999);

	return r;
}

taoNodeRoot* taoWorld::unlinkFree(taoNodeRoot* root, taoNode* node, deFloat inertia, deFloat damping)
{
	deVector6 v = *node->velocity();
	taoNodeRoot* r = new taoNodeRoot(*node->frameGlobal());

	node->unlink();

	if (!root->getDChild())
		delete removeRoot(root->getID());

	deFrame home;
	home.identity();

	node->link(r, &home);

	taoJointDOF1* joint;

	joint = new taoJointPrismatic(TAO_AXIS_X);
	joint->setDamping(damping);
	joint->setInertia(inertia);
	joint->setDVar(new taoVarDOF1);
	joint->reset();
	joint->getVarDOF1()->dq_ = v[0][TAO_AXIS_X];
	node->addJoint(joint);

	joint = new taoJointPrismatic(TAO_AXIS_Y);
	joint->setDamping(damping);
	joint->setInertia(inertia);
	joint->setDVar(new taoVarDOF1);
	joint->reset();
	joint->getVarDOF1()->dq_ = v[0][TAO_AXIS_Y];
	node->addJoint(joint);

	joint = new taoJointPrismatic(TAO_AXIS_Z);
	joint->setDamping(damping);
	joint->setInertia(inertia);
	joint->setDVar(new taoVarDOF1);
	joint->reset();
	joint->getVarDOF1()->dq_ = v[0][TAO_AXIS_Z];
	node->addJoint(joint);

	taoJointSpherical* joint2 = new taoJointSpherical();
	joint2->setDamping(damping);
	joint2->setInertia(inertia);
	joint2->setDVar(new taoVarSpherical);
	joint2->reset();
	joint2->getVarSpherical()->dq_ = v[1];
	node->addJoint(joint2);

	node->addABNode();
	taoDynamics::initialize(r);

	addRoot(r, -999);

	return r;
}

void taoWorld::sync(taoNodeRoot* root, deFloat time)
{
	root->sync();

	taoDynamics::initialize(root);
}
